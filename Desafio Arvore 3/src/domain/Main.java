package domain;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

public class Main {

	private static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		Arvore raiz = null;
		Integer esc, numero, achou = null;

		do {
			System.out.println("(1) Preencher a �rvore \n(2) Consultar pares \n(3) Consultar impares \n(4) Sair ");
			esc = scan.nextInt();
			scan.nextLine();
			switch(esc) {
			case 1: {
				raiz = null;
				for(int i = 1; i <= 10; i++) {
					System.out.println("Informe o n�mero que deseja inserir: ");
					numero = scan.nextInt();
					scan.nextLine();

					raiz = inserir(raiz, numero);
				}
				break;
			}
			case 2: {
				if(raiz == null) {
					System.out.println("Sua �rvore est� vazia!");
				} else {
					pares(raiz, achou);
				}
				break;
			}
			case 3: {
				if(raiz == null) {
					System.out.println("Sua �rvore est� vazia!");
				} else {
					impares(raiz, achou);
				}
				break;
			}
			default:{
				System.out.println("At�!");
			}
			}
		} while (esc != 4);
	}
	
	public static Arvore inserir(Arvore aux, Integer num) {
		if(aux == null) {
			aux = new Arvore();
			aux.numero = num;
			aux.direita = null;
			aux.esquerda = null;
		} else if(num < aux.numero) {
			aux.esquerda = inserir(aux.esquerda, num);
		} else {
			aux.direita = inserir(aux.direita, num);
		}
		return aux;
	}
	
	public static Integer pares(Arvore aux, Integer achou) {
		Stack<Integer> pilha = new Stack<Integer>();
		if(aux != null) {
			if(aux.numero %2 == 0) {
				achou = 1;
				System.out.println("Os n�meros pares s�o: " + aux.numero + " ");
			}
			achou = pares(aux.esquerda, achou);
			achou = pares(aux.direita, achou);
			pilha.add(achou);
		}
		return achou;
	}

	public static Integer impares(Arvore aux, Integer achou) {
		Queue<Integer> fila = new LinkedList<>();
		if (aux != null) {
			if (aux.numero % 2 != 0) {
				System.out.println("Os n�meros �mpares s�o: " + aux.numero + " ");
				achou = 1;
			}
			achou = impares(aux.esquerda, achou);
			achou = impares(aux.direita, achou);
			fila.add(achou);
		}
		return achou;
	}

}