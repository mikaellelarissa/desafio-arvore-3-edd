package domain;

public class Arvore {

	public Integer numero;
	public Arvore direita;
	public Arvore esquerda;
	
	public Arvore() {
		
	}
	
	public Arvore(Integer numero, Arvore direita, Arvore esquerda) {
		this.numero = numero;
		this.direita = direita;
		this.esquerda = esquerda;
	}
	
	public Integer getNumero() {
		return numero;
	}
	
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	
	public Arvore getDireita() {
		return direita;
	}
	
	public void setDireita(Arvore direita) {
		this.direita = direita;
	}
	
	public Arvore getEsquerda() {
		return esquerda;
	}
	
	public void setEsquerda(Arvore esquerda) {
		this.esquerda = esquerda;
	}
	
}
